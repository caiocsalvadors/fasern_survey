<div class="col-lg-3">
    <div class="panel panel-default sidebar">
        <ul class="list-group">
            <li class="list-group-item list-group-item-action"><a href="{{ url('/surveys') }}"><i class="fa fa-newspaper-o" aria-hidden="true"></i>Tipos de Pesquisas </a></li>
            <li class="list-group-item list-group-item-action"><a href="{{ url('/question_categories') }}"><i class="fa fa-tags" aria-hidden="true"></i>Categorias das perguntas</a></li>
            <li class="list-group-item list-group-item-action"><a href="{{ url('/questions') }}"><i class="fa fa-question-circle-o" aria-hidden="true"></i>Perguntas</a></li>
            <li class="list-group-item list-group-item-action"><a href="{{ url('/results') }}"><i class="fa fa-list" aria-hidden="true"></i>Resultados</a></li>
            <li class="list-group-item list-group-item-action"><a href="{{ url('/reports') }}"><i class="fa fa-bar-chart" aria-hidden="true"></i>Relatorio</a></li>
        </ul>
    </div>
</div>