@extends('layouts.app')

@section('content')
<? var_dump('success') ?>
<div class="container">
    <div class="row">        
        @include('shared.sidebar')
        <div class="col-lg-9">
            <div class="panel panel-default">
                <div class="panel-heading">Relatórios</div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="panel-body">
                    <h2>Pesquisas</h2>
                    <h5>Quantidade de pesquisas realizadas: {{ $counter }}</h5>
                    <table id="table" class="display table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Quantidade de respostas</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Nome</th>
                                <th>Quantidade de respostas</th>
                            </tr>
                        </tfoot>
                        <tbody>                    
                            @if($surveys)
                                @foreach($surveys as $survey)
                                <tr>
                                    <td>{{ $survey->name }}</td>
                                    <td>{{ $survey->results_count }}</td>
                                    <!--<td>
                                        <a href="{{ route('report.surveyReport', $survey->id) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </td>-->
                                </tr>                                    
                                @endforeach
                            @else
                                nada
                            @endif            
                        </tbody>
                    </table>
                    <h2>Respostas de cada pergunta</h2>
                    <table id="table2" class="display table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Titulo da Pergunta</th>
                                <th>Quantidade de respostas</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Titulo da Pergunta</th>
                                <th>Quantidade de respostas</th>
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                        <tbody>                    
                            @if($surveys)
                                @foreach($questions as $question)
                                <tr>
                                    <td>{{ $question->title }}</td>
                                    <td>{{ $question->answers_count }}</td>
                                    <td>
                                        <a href="{{ route('report.questionsReport', $question->id) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </td>
                                </tr>                                    
                                @endforeach
                            @else
                                nada
                            @endif            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
