@extends('layouts.app')

@section('content')
<? var_dump('success') ?>
<div class="container">
    <div class="row">        
        @include('shared.sidebar')
        <div class="col-lg-9 show">
            <div class="panel panel-default">
                <div class="panel-heading">Relatórios - Pergunta</div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="panel-body">
                    <h2>{{ $question->title }}</h2>
                    <div class="row">
                        @foreach($optionsWithCounter as $option)
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <h3>{{ $option["option"] }}</h3>
                                <p>{{ $option["counter"] }} - {{ $option["percentage"] }}%</p>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection