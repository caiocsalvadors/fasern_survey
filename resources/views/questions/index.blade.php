@extends('layouts.app')

@section('content')
<? var_dump('success') ?>
<div class="container">
    <div class="row">        
        @include('shared.sidebar')
        <div class="col-lg-9">
            <div class="panel panel-default">
                <div class="panel-heading">Perguntas <a href="{{ url('/questions/create') }}" class="add">Adicionar</a></div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="panel-body">
                    <table id="table" class="display table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Categoria</th>
                                <th>Tipo</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Title</th>
                                <th>Categoria</th>
                                <th>Tipo</th>
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                        <tbody>                    
                            @if($questions)
                                @foreach($questions as $question)
                                <tr>
                                    <td>{{ $question->title }}</td>
                                    <td>{{ $question->category->name }}</td>
                                    <td>{{ $question->type->name }}</td>
                                    <td>
                                        <a href="{{ route('questions.edit',$question->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['questions.destroy', $question->id], 'class' => 'formDelete']) !!}

                                        {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', array('type' => 'submit', 'class' => 'delete')) !!}

                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                nada
                            @endif            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
