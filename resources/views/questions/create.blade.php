@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">        
        @include('shared.sidebar')
        <div class="col-lg-9">
            <div class="panel panel-default">
                <div class="panel-heading">Nova Pergunta</div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="panel-body">
                    {!! Form::open(['action' => 'QuestionController@store']) !!}
                        <?=Form::token();?>
                        <div class="form-group">
                            <?=Form::label('title', 'Título:');?>
                            <?=Form::text('title', null, ['class' => 'form-control']);?>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    {!! Form::label('question_type_id', 'Tipo:') !!}
                                    {!! Form::select('question_type_id', $types, null, ['class'=>'form-control questionType', 'placeholder' => 'Escolha um tipo']) !!}
                                </div>
                                <div class="col-lg-6">
                                    {!! Form::label('question_category_id', 'Categoria:') !!}
                                    {!! Form::select('question_category_id', $categories, null, ['class'=>'form-control', 'placeholder' => 'Escolha uma categoria']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group questionOptions">
                            <?=Form::label('options', 'Opções (uma por linha):');?>
                            <?=Form::textarea('options', null, ['class' => 'form-control']);?>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                {!! Form::hidden('is_required', 0) !!}
                                <!--<div class="col-lg-6">
                                    {!! Form::label('is_required', 'Obrigatório?') !!}
                                    {!! Form::checkbox('is_required', 1) !!}
                                </div>-->
                                <div class="col-lg-6">
                                    {!! Form::label('extra', 'Tem pergunta extra?') !!}
                                    {!! Form::checkbox('extra', 1, null, ['class'=>'questionPerguntaExtra']) !!}
                                </div>
                            </div>                            
                        </div>
                        <div class="form-group questionExtra">
                            <?=Form::label('extra_text', 'Pergunta extra:');?>
                            <?=Form::text('extra_text', null, ['class' => 'form-control']);?>
                        </div>
                        <a href="{{ url('/questions') }}" class="btn btn-danger back">Voltar</a>
                        <?=Form::submit('Cadastrar', ['class' => 'btn btn-primary']);?>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
