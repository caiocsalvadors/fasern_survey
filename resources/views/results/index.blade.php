@extends('layouts.app')

@section('content')
<? var_dump('success') ?>
<div class="container">
    <div class="row">        
        @include('shared.sidebar')
        <div class="col-lg-9">
            <div class="panel panel-default">
                <div class="panel-heading">Resultados</div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="panel-body">
                    <table id="table" class="display table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Numeracão</th>
                                <th>Nome</th>
                                <th>CPF</th>
                                <th>Pesquisa</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Numeracão</th>
                                <th>Nome</th>
                                <th>CPF</th>
                                <th>Pesquisa</th>
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                        <tbody>                    
                            @if($results)
                                @foreach($results as $result)
                                <tr>
                                    <td>{{ $result->id }}</td>
                                    <td>{{ $result->person->name }}</td>
                                    <td>{{ $result->person->cpf }}</td>
                                    <td>{{ $result->survey->name }}</td>
                                    <td>
                                        <a href="{{ route('results.show',$result->id) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['results.destroy', $result->id], 'class' => 'formDelete']) !!}

                                        {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', array('type' => 'submit', 'class' => 'delete')) !!}

                                        {!! Form::close() !!}
                                    </td>
                                </tr>                                    
                                @endforeach
                            @else
                                nada
                            @endif            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
