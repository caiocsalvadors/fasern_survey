@extends('layouts.app')

@section('content')
<? var_dump('success') ?>
<div class="container">
    <div class="row">        
        @include('shared.sidebar')
        <div class="col-lg-9">
            <div class="panel panel-default">
                <div class="panel-heading">Resultado número: {{$result->id}}</div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Nome</h3>
                            <p>{{ $result->person->name }}</p>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <h3>CPF</h3>
                            <p>{{ $result->person->cpf }}</p>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <h3>Email</h3>
                            <p>{{ $result->person->email }}</p>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <h3>Pesquisa</h3>
                            <p>{{ $result->survey->name }}</p>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <h3>Data e Hora</h3>
                            <p>{{ $result->created_at->format('d-m-Y  \à\s  H:i') }}</p>
                        </div>
                        <!--<div class="col-md-12">
                            <h2>Respostas</h2>
                            @php
                                //var_dump($questions);
                            @endphp
                        </div>-->
                        <div class="col-md-12">
                            <h2>Respostas</h2>
                            <div class="row show">
                            @php
                                $title = null;
                            @endphp
                            @foreach($result->answers as $index => $answer) 
                                @if($title == $answer->question->title)                                    
                                    <div class="col-md-12 col-sm-12">
                                        <p>{{ $answer->answer }}</p>
                                         @if($answer->extra_text)
                                            <h4>{{$answer->extra_text}}</h4>
                                            <small>{{$answer->extra_answer}}</small>
                                        @endif   
                                    </div>    
                                @else
                                    <div class="col-md-12 col-sm-12">
                                        <h3>{{ $answer->question->title }}</h3>
                                        <p>{{ $answer->answer }}</p>
                                        @if($answer->extra_text)
                                            <h4>{{$answer->extra_text}}</h4>
                                            <small>{{$answer->extra_answer}}</small>
                                        @endif       
                                    </div>                          
                                @endif
                                @php
                                    $title = $answer->question->title;
                                @endphp
                            @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
