<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>FASERN - Pesquisa de satisfação</title>


        <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
      </head>
    <body>
        <div class="site forms">
            <div class="container">
                <h2>{{$survey->name}}</h2>
                <p>A Fasern está sempre se atualizando para oferecer o melhor para você e para o seu futuro. A partir deste ano, estamos inovando e disponibilizando a pesquisa online através de sistema contratado, garantindo o total sigilo quanto ao campo de CPF, utilizado apenas para que não haja duplicidade na resposta do questionário. Para isto, as pesquisas são automaticamente direcionadas para a empresa contratada, não sendo dado acesso à Fasern sobre quem respondeu a pesquisa, afinal o nosso objetivo é saber a sua opinião, pois ela é decisiva para que melhoremos cada vez mais os nossos serviços e, além disso, vale prêmios muito especiais para você.<br><br>
                Participe e boa sorte!</p>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @php
                    $category = 'Informacoes'
                @endphp
                <div class="row">
                    <h3>{{$category}}</h3>
                    {!! Form::open(['action' => 'HomeController@sendForm']) !!}

                    {{ Form::hidden('survey', $survey->id) }}

                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <p><b>Nome</b></p>
                        {{ Form::text('name', null, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <p><b>CPF (Somente os números)</b></p>
                        {{ Form::text('cpf', null, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <p><b>E-mail</b></p>
                        {{ Form::text('email', null, ['class' => 'form-control']) }}
                    </div>
                    @foreach($questions as $question)
                        @if (!($category == $question->category->name))                        
                            @php
                                $category = $question->category->name
                            @endphp
                            <h3><?= $category?></h3>
                        @endif
                        <!-- IF is Text -->
                        @if($question->type->name == 'text')
                            <div class="col-sm-12">
                                <p><b>{{$question->title}}</b></p>
                                {{ Form::text($question->id, null, ['class' => 'form-control']) }}
                                @if($question->extra)
                                    <p><b>{{$question->extra_text}}</b></p>
                                    {{ Form::text('extra_'.$question->id, null, ['class' => 'form-control']) }}
                                @endif
                            </div>
                        @endif
                        <!-- IF is Radio -->
                        @if($question->type->name == 'radio')
                            <div class="col-sm-12">
                                <p><b>{{$question->title}}</b></p>
                                @php
                                $options= explode(PHP_EOL, $question->options);
                                @endphp
                                @foreach($options as $option)
                                    {{ Form::radio($question->id, $option) }}
                                    {{ Form::label($question->id, $option) }}
                                @endforeach
                                @if($question->extra)
                                    <p><b>{{$question->extra_text}}</b></p>
                                    {{ Form::text('extra_'.$question->id, null, ['class' => 'form-control']) }}
                                @endif
                            </div>
                        @endif
                        <!-- IF is Checkbox -->
                        @if($question->type->name == 'check')
                            <div class="col-sm-12">
                                <p><b>{{$question->title}}</b></p>
                                @php
                                $options= explode("\n", $question->options);
                                @endphp
                                @foreach($options as $option)
                                    <div class="form-group">
                                        {{ Form::checkbox($question->id.'[]', $option) }}
                                        {{ Form::label($question->id, $option) }}
                                    </div>
                                @endforeach
                                @if($question->extra)
                                    <p><b>{{$question->extra_text}}</b></p>
                                    {{ Form::text('extra_'.$question->id, null, ['class' => 'form-control']) }}
                                @endif
                            </div>
                        @endif
                    @endforeach
                    {{ Form::submit('Cadastrar', ['class' => 'btn btn-primary']) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>