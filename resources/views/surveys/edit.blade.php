@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">        
        @include('shared.sidebar')
        <div class="col-lg-9">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Pesquisa</div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="panel-body">
                    {!! Form::model($survey, ['method' => 'PATCH', 'route' => ['surveys.update', $survey->id]]) !!}
                        <?=Form::token();?>
                        <div class="form-group">
                            <?=Form::label('name', 'Nome');?>
                            <?=Form::text('name', null, ['class' => 'form-control']);?>
                        </div>
                        <div class="form-group">
                            <?=Form::label('questions', 'Perguntas');?>
                            <?=Form::select('questions[]', $questions, $survey->questions->pluck('id'), ['id' => 'questions', 'multiple' => 'multiple', 'class' => 'form-control']);?>
                        </div>
                        <a href="{{ url('/surveys') }}" class="btn btn-danger back">Voltar</a>
                        <?=Form::submit('Cadastrar', ['class' => 'btn btn-primary']);?>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
