@extends('layouts.app')

@section('content')
<? var_dump('success') ?>
<div class="container">
    <div class="row">        
        @include('shared.sidebar')
        <div class="col-lg-9">
            <div class="panel panel-default">
                <div class="panel-heading">Categorias das perguntas <a href="{{ url('/question_categories/create') }}" class="add">Adicionar</a></div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="panel-body">
                    <table id="table" class="display table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nome</th>
                                <th>Prioridade</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Id</th>
                                <th>Nome</th>
                                <th>Prioridade</th>
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                        <tbody>                    
                            @if($questionCategories)
                                @foreach($questionCategories as $questionCategory)
                                <tr>
                                    <td>{{ $questionCategory->id }}</td>
                                    <td>{{ $questionCategory->name }}</td>
                                    <td>{{ $questionCategory->priority }}</td>
                                    <td>
                                        <a href="{{ route('question_categories.edit',$questionCategory->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['question_categories.destroy', $questionCategory->id], 'class' => 'formDelete']) !!}

                                        {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', array('type' => 'submit', 'class' => 'delete')) !!}

                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                nada
                            @endif            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
