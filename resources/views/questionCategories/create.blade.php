@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">        
        @include('shared.sidebar')
        <div class="col-lg-9">
            <div class="panel panel-default">
                <div class="panel-heading">Nova Categoria</div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="panel-body">
                    {!! Form::open(['action' => 'QuestionCategoryController@store']) !!}
                        <?=Form::token();?>
                        <div class="form-group">
                            <?=Form::label('name', 'Nome');?>
                            <?=Form::text('name', null, ['class' => 'form-control']);?>
                        </div>
                        <div class="form-group">
                            <?=Form::label('priority', 'Prioridade');?>
                            <?php
                                echo Form::select('priority',[
                                '0' => 0,
                                '1' => 1,
                                '2' => 2,
                                '3' => 3,
                                '4' => 4,
                                '5' => 5,
                                '6' => 6,
                                '7' => 7,
                                '8' => 8,
                                '9' => 9,
                                '10' => 10,
                                ],
                                null,
                                ['class' => 'form-control']
                                );
                            ?>
                        </div>
                        <a href="{{ url('/question_categories') }}" class="btn btn-danger back">Voltar</a>
                        <?=Form::submit('Cadastrar', ['class' => 'btn btn-primary']);?>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
