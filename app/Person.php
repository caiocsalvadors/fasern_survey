<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    public $table = 'persons';
    public $timestamps = false;
    
    protected $fillable = [
        'name', 'cpf', 'email'
    ];
}
