<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    public $timestamps = false;
    public $fillable = [
        'id_question', 'id_result', 'answer'
    ];

    public function result(){
        return $this->belongsTo('App\Result');
    }

    public function question(){
        return $this->belongsTo('App\Question');
    }
}
