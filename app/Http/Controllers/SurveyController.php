<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Survey;
use App\Question;

class SurveyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $surveys = Survey::all();
        return view('surveys.index', compact('surveys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $questions = Question::pluck('title', 'id');
        return view('surveys.create', compact('questions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:surveys'
        ]);
        $input = $request->all();
        $question = Survey::create($request->all());
        if(array_key_exists('questions', $input)){
            $question->questions()->attach($input['questions']);
        }
        return redirect()->route('surveys.index')
            ->with('success', 'Tipo de pesquisa criado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $questions = Question::pluck('title', 'id');
        $survey = Survey::find($id);
        return view('surveys.edit',compact('survey', 'questions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:surveys,name,'.$id,
        ]);
        
        $input = $request->all();
        $survey = Survey::find($id);
        $survey->update($request->all());
        if(array_key_exists('questions', $input)){
            $survey->questions()->sync($input['questions']);
        }        

        return redirect()->route('surveys.index')
                        ->with('success','Pesquisa atualizada com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $survey = Survey::findOrFail($id);
        $survey->questions()->detach();
        $survey->delete();

        return redirect()->route('surveys.index')
            ->with('success','Pesquisa deletada!');
    }
}
