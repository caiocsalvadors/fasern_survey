<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Answer;
use App\Person;
use App\Question;
use App\Result;
use App\Survey;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $counter = Result::count();
        $surveys = Survey::withCount('results')->get();
        $questions = Question::withCount('answers')->get();
        return view('reports.index', compact('counter', 'surveys', 'questions'));
    }

    public function surveyReport($survey_id){
        $survey = Survey::find($survey_id);
        return view('reports.survey', compact('survey'));
    }

    public function questionsReport($question_id){
        $question = Question::find($question_id);      
        $options = explode(PHP_EOL, $question->options);
        $optionsWithCounter = [];
        $qtd_answers = count($question->answers);
        foreach($options as $key=>$option){            
            $optionsWithCounter[$key]['option'] = $option;
            $optionsWithCounter[$key]['counter'] = 0;
            $optionsWithCounter[$key]['percentage'] = 0;
            foreach($question->answers as $answer){
               if($answer->answer == $option){
                   $optionsWithCounter[$key]['counter']++;             
               }
           }           
           $optionsWithCounter[$key]['percentage'] =  (($optionsWithCounter[$key]['counter'] * 100) / $qtd_answers);
           $optionsWithCounter[$key]['percentage'] = number_format((float)$optionsWithCounter[$key]['percentage'], 2, '.', '');
        }        
        return view('reports.question', compact('question', 'optionsWithCounter'));
    }
}
