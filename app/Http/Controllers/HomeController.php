<?php

namespace App\Http\Controllers;

use App\Survey;
use App\Person;
use App\Result;
use App\Answer;
use App\Question;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct() {
        $this->middleware('auth', ['only' => ['index']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function survey($id){
        $survey = Survey::findOrFail($id);
        $questions = $survey->getOrdenedQuestions();
        return view('survey', compact('survey', 'questions'));
    }

    public function sendForm(Request $request){
        $request->validate([
            'name' => 'required|unique:persons',
            'cpf' => 'required|unique:persons',
            'email' => 'required|unique:persons'
        ]);

        //Person data
        $name = $request['name'];
        $cpf = $request['cpf'];
        $email = $request['email'];        
        $person = Person::create([
            'name' => $name,
            'cpf' => $cpf,
            'email' => $email
        ]);
        
        //Result data
        $result = new Result;
        $result->survey_id = $request['survey'];
        $result->person_id = $person->id;
        $result->save();
        
        //Registering answers
        $input = $request->except('_token', 'survey', 'name', 'cpf', 'email');  
        foreach($input as $form_id=>$question_answer){
            $question = Question::find($form_id);
            $checkExtra = true;
            //If Checkbox
            if(is_array($question_answer)){
                $len = count($question_answer);
                //$j = 0;
                foreach($question_answer as $index => $answer_item){    
                    $checkExtra = false;
                    if ($index == $len - 1) {
                        $checkExtra = true;
                    }                
                    /*if($j != 0){
                        $checkExtra = false;
                    }  */                  
                    $this->createAnswer($input, $question, $result, $answer_item, $checkExtra);
                    //$j++;
                }
            }
            //If is not Checkbox
            else{
                //Just enter if is a question not for extras
                if(is_numeric($form_id)){     
                    $this->createAnswer($input, $question, $result, $question_answer, $checkExtra);
                }
            }
        }

        /*$this->sendEmail($person->email, $result->id);*/
        
        return view('thanks');
    }

    public function createAnswer($input, $question, $result, $answer_text, $checkExtra){
        $answer = new Answer;
        if($question->extra && $checkExtra){
            if(array_key_exists('extra_'.$question->id , $input)){
                $answer->extra_text = $question->extra_text;
                $answer->extra_answer = $input['extra_'.$question->id];
            }
        }
        $answer->question_id = $question->id;
        $answer->result_id = $result->id;
        $answer->answer = $answer_text;
        $answer->save();
    }

    public function sendEmail($number, $email){
        $userEmail = $email;
        $msg = "ATENÇÃO, O NÚMERO DO SEU FORMULÁRIO É ".$number." ESTE NÚMERO SERÁ UTILIZADO NO SORTEIO.";

        Mail::raw($msg, function ($message) use ($emailusuario) {
            $message->to($emailusuario)->subject('Número do formulário - Pesquisa de satisfação FASERN');
        });
    }
}
