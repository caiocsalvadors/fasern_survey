<?php

namespace App\Http\Controllers;

use App\Question;
use App\QuestionCategory;
use App\QuestionType;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $questions = Question::all();        
        return view('questions.index', compact('questions', 'categories', 'types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = QuestionCategory::pluck('name', 'id');              
        $types = QuestionType::pluck('name', 'id');
        return view('questions.create', compact('questions', 'categories', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:questions',
            'question_type_id' => 'required',
            'question_category_id' => 'required'
        ]);

        Question::create($request->all());
        return redirect()->route('questions.index')
            ->with('success', 'Pergunta criada com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::find($id);
        $type = $question->question_type_id;
        $categories = QuestionCategory::pluck('name', 'id');              
        $types = QuestionType::pluck('name', 'id');
        return view('questions.edit', compact('question', 'categories', 'types', 'type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'title' => 'required|unique:questions,title,'.$id,
            'question_type_id' => 'required',
            'question_category_id' => 'required'
        ]);
        Question::find($id)->update($request->all());
        return redirect()->route('questions.index')
                        ->with('success','Pergunta atualizada com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Question::findOrFail($id)->delete();

        return redirect()->route('questions.index')
            ->with('success','Pergunta deletada!');
    }
}
