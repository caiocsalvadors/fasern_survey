<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $fillable = [
        'survey_id', 'person_id'
    ];

    public function person(){
        return $this->belongsTo('App\Person');
    }

    public function survey(){
        return $this->belongsTo('App\Survey');
    }

    public function answers(){
        return $this->hasMany('App\Answer');
    }
}
