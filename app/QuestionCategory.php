<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionCategory extends Model
{
    public $table = 'question_categories';
    public $timestamps = false;
    protected $fillable = ['name', 'priority'];
}
