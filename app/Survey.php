<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    public $timestamps = false;
    //
    protected $fillable = [
        'name'
    ];

    public function questions(){
        return $this->belongsToMany('App\Question');
    }

    public function results(){
        return $this->hasMany('App\Result');
    }

    public function getOrdenedQuestions(){
        $questions = $this->questions;         
        $questions = $questions->sortByDesc(function($question){
            return $question->category->priority;
        });
        return $questions;
    }
}
