<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'question_type_id', 'question_category_id', 'title', 'options', 'is_required', 'extra', 'extra_text'
    ];

    public function category(){
        return $this->belongsTo('App\QuestionCategory', 'question_category_id');
    }
    
    public function type(){
        return $this->belongsTo('App\QuestionType', 'question_type_id');
    }

    public function surveys(){
        return $this->belongsToMany('App\Survey');
    }

    public function answers(){
        return $this->hasMany('App\Answer');
    }
}
