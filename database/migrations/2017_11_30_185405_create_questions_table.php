<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_type_id')->unsigned();
            $table->integer('question_category_id')->unsigned();
            $table->string('title');
            $table->text('options')->nullable();
            $table->boolean('extra')->default(0);
            $table->string('extra_text')->nullable();
            $table->boolean('is_required')->default(0);   
            $table->timestamps();
            
            $table->foreign('question_type_id')->references('id')->on('question_types');
            $table->foreign('question_category_id')->references('id')->on('question_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
