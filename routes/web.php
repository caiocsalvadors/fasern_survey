<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/pesquisa/{id}', 'HomeController@survey')->name('pesquisa');
Route::post('/sendForm', ['as'=>'sendForm', 'uses'=>'HomeController@sendForm']);

Route::resource('surveys', 'SurveyController');

Route::resource('question_categories', 'QuestionCategoryController');

Route::resource('questions', 'QuestionController');

Route::resource('results', 'ResultController');

Route::resource('reports', 'ReportController');
Route::get('/reports/survey/{id}', 'ReportController@surveyReport')->name('report.surveyReport');
Route::get('/reports/question/{id}', 'ReportController@questionsReport')->name('report.questionsReport');
